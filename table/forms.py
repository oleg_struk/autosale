from django import forms

class YearsFilterForm(forms.Form):
    min_year = forms.IntegerField(label='from', required=False)
    max_year = forms.IntegerField(label='to',required=False)