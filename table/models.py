from django.db import models


# Create your models here.
class Brand(models.Model):
    brand = models.CharField(max_length=15,default='',unique=True)
    def __unicode__ (self):
        return self.brand
    def __str__ (self):
         return self.brand
    # def get_brand_list(self):
    #     return Brand.objects.first()
class Model(models.Model):
    model = models.CharField(max_length=15, default='',unique=True)
    brand = models.ForeignKey('Brand', on_delete=models.CASCADE, null=True)
    def __str__ (self):
        return self.model

class AutoProfile(models.Model):
    class Meta:
        verbose_name_plural = 'Auto'
    id = models.AutoField(primary_key=True)
    brand_id = models.ForeignKey(Brand,on_delete=models.CASCADE,verbose_name='Brand')
    model_id = models.ForeignKey(Model,on_delete=models.CASCADE,verbose_name='Model')
    year = models.IntegerField(help_text='Input year of release')
    price = models.IntegerField(default=00000)
    owner_name = models.CharField(max_length=20,default='')
    category = models.CharField(max_length=20,default='', null=True, blank=True)
    def save(self, *args, **kwargs):
        if int(self.year) <= 2010 and int(self.year) >= 2001:
            self.category = 'from 2000 to 2010'
        elif int(self.year) > 2010:
            self.category = 'from 2010'
        elif int(self.year) < 2000 and int(self.year) > 1989:
            self.category = 'from 1990 to 2000'
        elif int(self.year) < 1990:
            self.category = 'to 1990'
        super(AutoProfile, self).save(*args, **kwargs)
