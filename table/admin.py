from django.contrib import admin
from table.models import AutoProfile, Brand, Model
# Register your models here.

class UserAdmin(admin.ModelAdmin):
    list_display = ('id','brand_id','model_id','year', 'price','owner_name')
    readonly_fields = ['category']
    list_select_related = True

admin.site.register(AutoProfile, UserAdmin)
admin.site.register(Brand)
admin.site.register(Model)


