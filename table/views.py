from django.shortcuts import render

from table.models import AutoProfile
from table.forms import YearsFilterForm

def home(request):
    cars = AutoProfile.objects.all()
    form = YearsFilterForm(request.GET)
    if form.is_valid():
        if form.cleaned_data["min_year"]:
            cars = cars.filter(year__gte=form.cleaned_data["min_year"])
        if form.cleaned_data["max_year"]:
            cars = cars.filter(year__lte=form.cleaned_data["max_year"])
    order_by = request.GET.get('order_by','')
    if order_by in ('brand__brand','model__model','category','price'):
        cars = AutoProfile.objects.order_by(order_by).all()
        if request.GET.get('reverse','') == '1':
            cars = cars.reverse()

    args = {'cars': cars, 'form':form}

    return render(request,'table/home.html',args)