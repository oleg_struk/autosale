
from django.contrib import admin
from django.conf.urls import url, include
from autosale.views import main_page_redirect
from rest_framework.schemas import get_schema_view

API_TITLE = 'Pastebin API'
API_DESCRIPTION = 'A Web API for creating and viewing highlighted code snippets.'
schema_view = get_schema_view(title=API_TITLE)

urlpatterns = [
    url(r'^', include('api.urls')),
    url(r'^$', main_page_redirect, name='main_page_redirect'),
    url(r'admin/', admin.site.urls),
    url(r'^table/', include('table.urls')),
    url(r'^api', include('api.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^schema/$', schema_view),
]
