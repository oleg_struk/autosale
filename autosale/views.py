from django.shortcuts import redirect

def main_page_redirect(request):
    return redirect('/table/')
