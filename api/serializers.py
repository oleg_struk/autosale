from rest_framework import serializers
from table.models import AutoProfile, Model , Brand
from django.contrib.auth.models import User


class ModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Model
        fields = ('id','model')

class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('id','brand')

class ApiSerializer(serializers.ModelSerializer):
    brand = serializers.StringRelatedField(source='brand_id.brand')
    model = serializers.StringRelatedField(source='model_id.model')

    class Meta:
        model = AutoProfile
        fields = ('id','brand_id','brand','model_id','model','year','price','owner_name','category')
        read_only = ('category')
        write_only_field = ('brand_id','model_id')

    def create(self, validated_data):
        obj = AutoProfile.objects.create(**validated_data)
        return obj


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


